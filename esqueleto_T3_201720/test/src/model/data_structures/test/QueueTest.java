package model.data_structures.test;

import junit.framework.TestCase;
import model.data_structures.Queue;


public class QueueTest<Item> extends TestCase
{
	private Queue<Item> queue;
	
	/**
	 * Escenario 1: Crea un queue vacío.
	 * @throws Exception 
	 */
	public void setupEscenario1() throws Exception 
	{
		try 
		{
			queue = new Queue<Item>();
		} 
		catch ( Exception e) 
		{
			throw new Exception("No se debería generar el error " + e.getMessage());
		}
	}
	
	/**
	 * Escenario 2: Crea un queue con elementos.
	 * @throws Exception 
	 */
	public void setupEscenario2() throws Exception 
	{
		try 
		{
			queue = new Queue<Item>();
			
			Item item = (Item) "item";
			Item item2 = (Item) "item2";
			Item item3 = (Item) "item3";
			
			queue.enqueue(item);
			queue.enqueue(item2);
			queue.enqueue(item3);
		} 
		catch ( Exception e) 
		{
			throw new Exception("No se debería generar el error " + e.getMessage());
		}
	}
	
	public void testSize() throws Exception
	{
		// 1
		setupEscenario1();
		assertEquals("El tamaño no corresponde.", 0, queue.size());
		
		// 2
		setupEscenario2();
		assertEquals("El tamaño no corresponde.", 3, queue.size());
				
	}
	
	public void testEnqueue() throws Exception
	{
		setupEscenario1();
    	// 1
    	try 
    	{
    		Item item = (Item) "item";
			queue.enqueue(item);
			assertEquals("No se agregó", 1, queue.size());
		} 
    	catch (Exception e) 
    	{
			throw new Exception("No se debería generar el error: " + e.getMessage());
		}
    	
    	// 2
    	
    	setupEscenario2();
    	try 
    	{
    		Item item4 = (Item) "item4";
    		queue.enqueue(item4);
			assertEquals("No se agregó", 4, queue.size());
		} 
    	catch (Exception e) 
    	{
    		throw new Exception("No se debería generar el error: " + e.getMessage());
		}
	}
	
	public void testDequeue() throws Exception
	{
		setupEscenario2();
    	// 1
    	queue.dequeue();
    	assertEquals("La cantidad de elementos no coincide.", 2, queue.size());
    	
	}

}
