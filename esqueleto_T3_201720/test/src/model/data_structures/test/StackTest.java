package model.data_structures.test;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest <Item > extends TestCase 
{
	private Stack<Item> stack;
	
	/**
	 * Escenario 1: Crea un stack vacío.
	 * @throws Exception 
	 */
	public void setupEscenario1() throws Exception 
	{
		try 
		{
			stack = new Stack<Item>();
		} 
		catch ( Exception e) 
		{
			throw new Exception("No se debería generar el error " + e.getMessage());
		}
	}
	
	/**
	 * Escenario 2: Crea un queue con elementos.
	 * @throws Exception 
	 */
	public void setupEscenario2() throws Exception 
	{
		try 
		{
			stack = new Stack<Item>();
			
			Item item = (Item) "item";
			Item item2 = (Item) "item2";
			Item item3 = (Item) "item3";
			
			stack.push(item);
			stack.push(item2);
			stack.push(item3);
		} 
		catch ( Exception e) 
		{
			throw new Exception("No se debería generar el error " + e.getMessage());
		}
	}
	
	public void testSize() throws Exception
	{
		// 1
		setupEscenario1();
		assertEquals("El tamaño no corresponde.", 0, stack.size());
		
		// 2
		setupEscenario2();
		assertEquals("El tamaño no corresponde.", 3, stack.size());
				
	}
	
	public void testPush() throws Exception
	{
		setupEscenario1();
    	// 1
    	try 
    	{
    		Item item = (Item) "item";
			stack.push(item);
			assertEquals("No se agregó", 1, stack.size());
		} 
    	catch (Exception e) 
    	{
			throw new Exception("No se debería generar el error: " + e.getMessage());
		}
    	
    	// 2
    	
    	setupEscenario2();
    	try 
    	{
    		Item item4 = (Item) "item4";
    		stack.push(item4);
			assertEquals("No se agregó", 4, stack.size());
		} 
    	catch (Exception e) 
    	{
    		throw new Exception("No se debería generar el error: " + e.getMessage());
		}
	}
	
	public void testPop() throws Exception
	{
		setupEscenario2();
    	// 1
    	stack.pop();
    	assertEquals("La cantidad de elementos no coincide.", 2, stack.size());
    	
	}
}
