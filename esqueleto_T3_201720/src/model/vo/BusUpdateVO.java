package model.vo;

/**
 * Representation of a bus update object
 */
public class BusUpdateVO 
{
	private String vehicleNo;
	private int tripId;
	private String routeNo;
	private String direction;
	private String destination;
	private double latitude;
	private double longitude;
	private String recordedTime;
	private RouteMap routeMap;
	
	private class RouteMap
	{
		private String href;

		public String getHref() 
		{
			return href;
		}

		public void setHref(String href) {
			
			this.href = href;
		}
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	public String getRouteNo() {
		return routeNo;
	}

	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getRecordedTime() {
		return recordedTime;
	}

	public void setRecordedTime(String recordedTime) {
		this.recordedTime = recordedTime;
	}

	public RouteMap getRouteMap() {
		return routeMap;
	}

	public void setRouteMap(RouteMap routeMap) {
		this.routeMap = routeMap;
	}
	
	
	 public String toString() 
	 {
	        return "BusUpdateVO{" + "VehicleNo:" + vehicleNo + ", TripId:" + tripId + ", RouteNo:" + routeNo + ", Direction:" + direction + ", Destrination:" + destination + ", Latitude" + latitude + "}";
	 }

}
