package model.vo;

public class StopVO 
{
	//
	// Attributes
	//
	private String stopId;
	private String stopCode;
	private String stopName;
	private String stopDesc;
	private String stopLat;
	private String stopLon;
	private String zoneId;
	private String stopUrl;
	private String locationType;
	
	//
	// Constructor
	//
	
	public StopVO( String stopId, String stopCode, String stopName, String stopDesc, String stopLat, String stopLon, String zoneId, String stopUrl, String locationType)
	{
		setStopId(stopId);
		setStopCode(stopCode);
		setStopName(stopName);
		setStopDesc(stopDesc);
		setStopLat(stopLat);
		setStopLon(stopLon);
		setZoneId(zoneId);
		setStopUrl(stopUrl);
		setLocationType(locationType);

	}

	public String getStopId() {
		return stopId;
	}

	public void setStopId(String stopId) {
		this.stopId = stopId;
	}

	public String getStopCode() {
		return stopCode;
	}

	public void setStopCode(String stopCode) {
		this.stopCode = stopCode;
	}

	public String getStopName() {
		return stopName;
	}

	public void setStopName(String stopName) {
		this.stopName = stopName;
	}

	public String getStopDesc() {
		return stopDesc;
	}

	public void setStopDesc(String stopDesc) {
		this.stopDesc = stopDesc;
	}

	public String getStopLat() {
		return stopLat;
	}

	public void setStopLat(String stopLat) {
		this.stopLat = stopLat;
	}

	public String getStopLon() {
		return stopLon;
	}

	public void setStopLon(String stopLon) {
		this.stopLon = stopLon;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getStopUrl() {
		return stopUrl;
	}

	public void setStopUrl(String stopUrl) {
		this.stopUrl = stopUrl;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
	
	public String toString() 
	{
		return  stopId +"," + stopCode + "," + stopName + "," + stopDesc + "," + stopLat + "," + stopLon + "," + zoneId + "," + stopUrl + "," + locationType + ","; 
	}

}
