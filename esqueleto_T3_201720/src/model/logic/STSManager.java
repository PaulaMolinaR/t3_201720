package model.logic;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

import model.data_structures.IStack;
import model.data_structures.Queue;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import api.ISTSManager;
import com.google.gson.*;

public class STSManager implements ISTSManager
{

	@Override
	public void readBusUpdate(File rtFile)  
	{
		
	    try 
	    {
	    	BufferedReader br = new BufferedReader(new FileReader(rtFile));
	    	String line = br.readLine();
	    	Gson gson = new GsonBuilder().create();
	    	
	    	while(line != null)
	    	{
	    		line = br.readLine();
	    		BusUpdateVO[] busUpdate = gson.fromJson(br, BusUpdateVO[].class);
	    		System.out.println( busUpdate );
	    	}

	    } 
	    catch (IOException e) 
	    {
	    	e.printStackTrace();
	    }
	       
		
	}


	@Override
	public IStack<StopVO> listStops(Integer tripID) 
	{
		
		ArrayList<BusUpdateVO> busUpdates = new ArrayList<BusUpdateVO>();
		ArrayList<StopVO> stops = new ArrayList<StopVO>();
		Stack<StopVO> listStops = new Stack<StopVO>(); 
		
		boolean encontrado = false;
		
		for(int i = 0; i < busUpdates.size() && encontrado == false; i++)
		{
			BusUpdateVO update = (BusUpdateVO) busUpdates.get(i);
			if(tripID == update.getTripId() )
			{
				double lat1 = update.getLatitude();
				double lon1 = update.getLongitude(); 
				encontrado = true;
			
				for(int j = 0; j < stops.size(); j++)
				{
					StopVO stop = (StopVO) stops.get(j);
					if( stop!=null )
					{
						double lat2 = Double.parseDouble(stop.getStopLat());
						double lon2 = Double.parseDouble(stop.getStopLon()); 
				
						double distance = getDistance(lat1, lon1, lat2, lon2);
						
						if(distance <= 70)
						{
							listStops.push(stop);
						}
					}
				
				}
		
			}	
		}
		return (IStack<StopVO>) listStops;
		
	}

	@Override
	public void loadStops( String stopsFile) throws IOException 
	{
		Queue<StopVO> stops = new Queue<StopVO>();
		
		BufferedReader br = new BufferedReader(new FileReader(stopsFile));
		String line = br.readLine();
		
		while(line != null)
		{
			line = br.readLine();
			String[] stop = line.split(",");

			for(int i = 0; i < stop.length; i++)
			{
				String stopId = stop[0];
				String stopCode = stop[1];
				String stopName = stop[2];
				String stopDesc = stop[3];
				String stopLat = stop[4];
				String stopLon = stop[5];
				String zoneId = stop[6];
				String stopUrl = stop[7];
				String locationType = stop[8];
				
				stops.enqueue(new StopVO(stopId, stopCode, stopName, stopDesc, stopLat, stopLon, zoneId, stopUrl, locationType));
				System.out.println(stopId +"," + stopCode + "," + stopName + "," + stopDesc + "," + stopLat + "," + stopLon + "," + zoneId + "," + stopUrl + "," + locationType + ",");			
			}			
		}	
		
		br.close();	
	}
	
	public double getDistance(double lat1, double lon1, double lat2, double lon2) 
	{
        // TODO Auto-generated method stub
        final int R = 6371*1000; // Radious of the earth
    
        Double latDistance = toRad(lat2-lat1);
        Double lonDistance = toRad(lon2-lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
                   Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
                   Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        Double distance = R * c;
         
        return distance;
 
    }

  private double toRad(Double value) 
  {
	  return value * Math.PI / 180;
  }
	
}
