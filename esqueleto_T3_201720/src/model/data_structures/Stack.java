package model.data_structures;

import java.util.Iterator;

public class Stack<Item> implements Iterable<Item>
{
	private Node first;
	private int size;
	
	private class Node
	{
		Item item;
		Node next;
	}
	
	public boolean isEmpty()
	{
		return first == null;
	}
	
	public int size()
	{
		return size;
	}
	
	
	public void push(Item item)
	{
		//Add item to top of stack 
		Node oldFirst = first;
		first = new Node();
		first.item = item;
		first.next = oldFirst;
		size++;
	}
	
	public Item pop()
	{
		//Remove item from top of stack
		Item item = first.item;
		first = first.next;
		size--;
		return item;
	}
	
	public Iterator<Item> iterator() 
	{
		return new ListIterator();
	}
	
	private class ListIterator implements Iterator<Item>
    {
        private Node current = first;
        
        public boolean hasNext()
        {
        	return current != null;  
        }
       
        public void remove() { }
        public Item next()
        {
        	Item item = current.item;
        	current = current.next;
        	return item;
        }
    } 

}
